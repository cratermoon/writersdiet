package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var adwords = []string{`ly`, `al`, `ive`, `ul`, `less`, `ant`, `ble`, `able`, `ic`, `ary`, `ac`, `ous`}
var be_verbs = []string{"am", "is", "are", "was", "were", "be", "been"}
var be_verb_suffixes = []string{"''m", "''re", "\\u2019m", "\\u2019re"}
var prepositions = []string{"about", "above", "across", "after", "against", "along", "among", "around", "at", "before", "behind", "below", "beneath", "beside", "between", "beyond", "by", "down", "during", "for", "from", "in", "inside", "into", "near", "of", "off", "on", "onto", "out", "outside", "over", "past", "through", "throughout", "to", "toward", "under", "underneath", "until", "up", "upon", "with", "within", "without"}
var waste_words = []string{"it", "this", "that", "there", "it\u2019s", "that\u2019s", "there\u2019s", "it's", "that's", "there's"}
var zombie_noun_suffixes = []string{"ion", "ions", "ism", "isms", "ity", "ities", "ment", "ments", "ness", "ance", "ances", "ence", "ences"}

type wordcounts struct {
	total        int
	adwords      int
	be_verbs     int
	prepositions int
	waste_words  int
	zombie_nouns int
}

func main() {
	var c wordcounts
	s := bufio.NewScanner(os.Stdin)
	s.Split(bufio.ScanWords)
	for s.Scan() {
		c.total = c.total + 1
		tok := s.Text()
		if isPreposition(tok) {
			c.prepositions++
		}
		if isWasteWord(tok) {
			c.waste_words++
		}
		if isBeVerb(tok) {
			c.be_verbs++
		}
		if isAdword(tok) {
			c.adwords++
		}
		if isZombieNoun(tok) {
			c.zombie_nouns++
		}
	}
	fmt.Printf("%d total words\n", c.total)
	printMetric("ad words", c.adwords, c.total)
	printMetric("be verbs", c.be_verbs, c.total)
	printMetric("prepositions", c.prepositions, c.total)
	printMetric("waste words", c.waste_words, c.total)
	printMetric("zombie nouns", c.zombie_nouns, c.total)
}

func printMetric(kind string, n, total int) {
	pct := 100.0 * float32(n) / float32(total)
	fmt.Printf("%d %s %.1f%%\n", n, kind, pct)
}
func isPreposition(t string) bool {
	return in(t, prepositions)
}

func isWasteWord(t string) bool {
	return in(t, waste_words)
}

func isBeVerb(t string) bool {
	return in(t, be_verbs) || hasSuffix(t, be_verb_suffixes)
}

func isAdword(t string) bool {
	return hasSuffix(t, adwords)
}

func isZombieNoun(t string) bool {
	return hasSuffix(t, zombie_noun_suffixes)
}

func in(t string, words []string) bool {
	for _, w := range words {
		if strings.EqualFold(t, w) {
			return true
		}
	}
	return false

}

func hasSuffix(t string, suffixes []string) bool {
	for _, w := range suffixes {
		if strings.HasSuffix(t, w) {
			return true
		}
	}
	return false

}
